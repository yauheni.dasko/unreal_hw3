// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "unreal_hw3GameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class UNREAL_HW3_API Aunreal_hw3GameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
