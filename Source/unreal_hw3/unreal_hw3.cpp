// Copyright Epic Games, Inc. All Rights Reserved.

#include "unreal_hw3.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, unreal_hw3, "unreal_hw3" );
